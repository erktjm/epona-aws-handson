# 各種ツールの準備

本ハンズオンでは、以下の外部サービスおよびソフトウェアを利用します。

* AWS
* GitLab(SaaS)
* Terraform
* AWS CLI

## 前提条件

1. インターネット接続した状態で行って下さい
1. 作業想定時間は2時間です
1. 必ず環境構築を事前に完了させてください。研修中に実施する時間はありませんのでご注意ください

## 1. ソフトウェアのインストール

### Git

バージョン管理ツールをインストールしていただきます。

* [Windows](./install_git_win.md)
* [Mac](./install_git_mac.md)

### Terraform

Terraformはインフラを安全で効率的に構築・変更・バージョン管理するためのツールです。

* [Windows](./install_terraform_win.md)
* [Mac](./install_terraform_mac.md)
* [Linux](./install_terraform_linux.md)

### AWS CLI

AWS Command Line Interface (AWS CLI)は、コマンドを使用してAWSサービスとやり取りするためのツールです。  
インストールした環境で、動作確認が出来ればOKです。

* [AWS CLI バージョン 2 のインストール、更新、アンインストール](https://docs.aws.amazon.com/ja_jp/cli/latest/userguide/install-cliv2.html)

## 2. 外部サービスの準備

### GitLab(SaaS)

Gitリポジトリのホスティングサービスです。  
**アカウントを既にお持ちの方にも、ご確認いただく箇所がありますのでご注意ください。**

* [GitLabの準備](./gitlab.md)

### AWS

事前にお渡ししている2つのAWSアカウント情報（Deliverity環境 × 1、Runtime環境 × 1）をご用意ください。

* [AWSの準備](./aws.md)

以上で環境準備は完了です。お疲れさまでした。  
この後は [事前課題](./assignment.md) を実施してください。
