#!/bin/bash -eu

SCRIPT_DIR=$(cd $(dirname $0); pwd)

users="$@"
. ${SCRIPT_DIR}/common.sh

function delete_delivery_terraform_accounts() {
    local user_name=$1
    local base_path=$(git rev-parse --show-toplevel)
    local delivery_tfstate_bucket_name=${user_name}-delivery-terraform-tfstate
    local staging_tfstate_bucket_name=${user_name}-staging-terraform-tfstate
    local delivery_backend_bucket_name=${user_name}-delivery-backend
    local staging_backend_bucket_name=${user_name}-staging-backend

    # delivery環境、Staging環境のtfstate管理用S3バケットの内容を空に設定
    empty_s3_bucket ${delivery_tfstate_bucket_name}
    empty_s3_bucket ${staging_tfstate_bucket_name}

    delete_user_access_key ${user_name}

    echo "delete setup_terraform_accounts: delivery: start"
    cd ${base_path}/${user_name}/setup_terraform_accounts/delivery
    terraform init --backend-config=./backend.config
    terraform state rm 'module.delivery.aws_s3_bucket.backend_itself'
    terraform destroy -lock=false --auto-approve
    echo "delete setup_terraform_accounts: delivery: done"

    echo "delete s3 bucket: delivery_backend_bucket_name: start"
    delete_s3_bucket ${delivery_backend_bucket_name}
    echo "delete s3 bucket: delivery_backend_bucket_name: done"
}

function delete_staging_terraform_accounts() {
    local user_name=$1
    local base_path=$(git rev-parse --show-toplevel)
    local staging_backend_bucket_name=${user_name}-staging-backend

    echo "delete setup_terraform_accounts: staging: start"
    cd ${base_path}/${user_name}/setup_terraform_accounts/runtimes/staging
    terraform init --backend-config=./backend.config
    terraform state rm 'module.backend.aws_s3_bucket.tfstate'
    terraform destroy -lock=false --auto-approve
    echo "delete setup_terraform_accounts: staging: done"

    echo "delete s3 bucket: staging_backend_bucket_name: start"
    delete_s3_bucket ${staging_backend_bucket_name}
    echo "delete s3 bucket: staging_backend_bucket_name: done"
}

check_tools

for user in ${users}; do
  echo "delete_staging_terraform_accounts: start: ${user}"
  set_staging_user_credential ${user}
  delete_staging_terraform_accounts ${user}
  echo "delete_staging_terraform_accounts: done: ${user}"
  echo "delete_delivery_terraform_accounts: start: ${user}"
  set_delivery_user_credential ${user}
  delete_delivery_terraform_accounts ${user}
  echo "delete_delivery_terraform_accounts: done: ${user}"
done
